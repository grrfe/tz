# tz

Convert timezones on the commandline


## Usage

Supported formats for `-d`: `HH:mm` & `yyyy-MM-ddTHH:mm`

Supported formats for `-f`/`-t`: IANA time zones

```
grrfe@gitlab:~$ tz --help                                                    
usage: tz [-h] [-d DATETIME] [-f FROM_TZ] [-t TO_TZ]

Convert times between different timezones

optional arguments:
  -h, --help            show this help message and exit
  -d DATETIME, --datetime DATETIME
                        Time to convert
  -f FROM_TZ, --from-tz FROM_TZ
                        Input timezone
  -t TO_TZ, --to-tz TO_TZ
                        Timezone to convert to

```