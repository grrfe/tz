#!/usr/bin/python3
import argparse
from datetime import datetime

from backports.zoneinfo import ZoneInfo

__PATTERNS = ["%Y-%m-%dT%H:%M", "%Y-%m-%dT%H:%M:%S.%fZ", "%H:%M"]


def attempt_parse(_input: str, zone: str) -> (str, datetime):
    now = datetime.now()
    for pattern in __PATTERNS:
        try:
            _datetime = datetime.strptime(_input, pattern).replace(tzinfo=ZoneInfo(zone))
            if _datetime.year == 1900:
                _datetime = _datetime.replace(year=now.year, month=now.month, day=now.day)

            return pattern, _datetime
        except ValueError:
            pass


def parse_input():
    local_tz = datetime.utcnow().astimezone().tzinfo.__str__()

    parser = argparse.ArgumentParser(description="Convert times between different timezones")
    parser.add_argument("-d", "--datetime", help="Time to convert", default=datetime.now(), required=False)
    parser.add_argument("-f", "--from-tz", help="Input timezone", default=local_tz, required=False)
    parser.add_argument("-t", "--to-tz", help="Timezone to convert to", default=local_tz, required=False)

    args = parser.parse_args()

    if type(args.datetime) is str:
        pattern, dt = attempt_parse(args.datetime, args.from_tz)
    else:
        pattern, dt = __PATTERNS[0], args.datetime

    if dt is not None:
        print(dt.astimezone(ZoneInfo(args.to_tz)).strftime(pattern))


if __name__ == "__main__":
    parse_input()
